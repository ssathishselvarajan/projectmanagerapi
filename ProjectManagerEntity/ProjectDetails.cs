﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerEntity
{
           public class ProjectDetails
        {
            public string PROJECT_ID { get; set; }
            public string PROJECT_NAME { get; set; }
            public DateTime? START_DATE { get; set; }
            public DateTime? END_DATE { get; set;}
            public int? PRIORITY { get; set; }
            public int? NoOfTasks { get; set; }
            public int? NoOfTasksCompleted { get; set;}
            public USER Manager { get; set; }
        }

}
