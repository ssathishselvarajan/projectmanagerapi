﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagerEntity
{
    public class ProjectManagerEntity
    {

        public List<PROJECT> GetAllProjects()
        {
            using (ProjectManagerEntities pe = new ProjectManagerEntities())
            {
                return pe.PROJECTs.ToList();
            }
        }


        public List<TASKTABLE> GetTasksByID(string id)
        {
           using (ProjectManagerEntities pe = new ProjectManagerEntities())
            {
                if (pe.TASKTABLEs.Any(a => a.PROJECT_ID == id))
                {

                    return pe.TASKTABLEs.Where(a => a.PROJECT_ID == id).ToList();
                }
                else
                {
                    throw new Exception(string.Format("Project with the id - {0} could not be found", id));
                }
            }
        }

        public bool AddProject(ProjectDetails newProject)
        {
            using (ProjectManagerEntities pe = new ProjectManagerEntities())
            {
                pe.PROJECTs.Add(new PROJECT()
                {
                        PROJECT_ID = newProject.PROJECT_ID,
                        PROJECT1 =newProject.PROJECT_NAME,
                        START_DATE = newProject.START_DATE,
                        END_DATE =newProject.END_DATE,
                        PRIORITY = newProject.PRIORITY
                });
                pe.SaveChanges();



                //if (pe.USERS.Any(a => a. == newProject.))
                //{
                //    USER userToRemove = _db.USERS.FirstOrDefault(a => a.USER_ID == updateUser.USER_ID);
                //    _db.USERS.Remove(userToRemove);
                //    _db.USERS.Add(updateUser);
                //    _db.SaveChanges();
                //    return true;
                //}
                //else
                //{
                //    throw new Exception(string.Format("User with the name - {0} could not be found", updateUser.USER_ID));
                //}


                return true;
            }

            

        }

        public List<ProjectDetails> GetProjectFromDetails()
        {

            List<ProjectDetails> retVal = new List<ProjectDetails>();
            List<PROJECT> allProjects = new List<PROJECT>();
            List<TASKTABLE> allTasks = new List<TASKTABLE>();
            List<USER> allUsers = new List<USER>();
            allProjects =GetAllProjects();
            allTasks = GetAllTasks();
            allUsers = GetAllUsers();
            foreach (PROJECT selectedProject in allProjects)
            {
                ProjectDetails pd = new ProjectDetails();
                pd.PROJECT_ID = selectedProject.PROJECT_ID;
                pd.PROJECT_NAME = selectedProject.PROJECT1;
                pd.START_DATE = selectedProject.START_DATE;
                pd.END_DATE = selectedProject.END_DATE;
                pd.PRIORITY = selectedProject.PRIORITY;
                if (allTasks.Any(a => a.PROJECT_ID == selectedProject.PROJECT_ID))
                {
                    var projectTasks = allTasks.Where(a => a.PROJECT_ID == selectedProject.PROJECT_ID).ToList();
                    pd.NoOfTasks = projectTasks.Count;
                    pd.NoOfTasksCompleted = projectTasks.Where(a => a.STATUS == true).Count();
                }
                if (allUsers.Any(a => a.PROJECT_ID == selectedProject.PROJECT_ID))
                {
                    pd.Manager = allUsers.Where(a => a.PROJECT_ID == selectedProject.PROJECT_ID).LastOrDefault();
                }
                retVal.Add(pd);
            }

            return (retVal);
        }

        public bool UpdateTask(TASKTABLE taskToUpdate)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                TASKTABLE updateTask = _db.TASKTABLEs.FirstOrDefault(a => a.TASK_ID == taskToUpdate.TASK_ID);
                if (updateTask != null)
                {
                    _db.TASKTABLEs.Remove(updateTask);
                    _db.TASKTABLEs.Add(taskToUpdate);
                    _db.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToUpdate.TASK));
                }
            }
            return true;
        }

        public bool DeleteTask(string id)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                TASKTABLE taskToDelete = _db.TASKTABLEs.FirstOrDefault(a => a.TASK_ID == id);
                if (taskToDelete != null)
                {
                    _db.TASKTABLEs.Remove(taskToDelete);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToDelete.TASK));
                }
            }
        }

        public PROJECT GetProjectByID(string id)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                if (_db.PROJECTs.Any(a => a.PROJECT_ID == id))
                {
                    return _db.PROJECTs.FirstOrDefault(a => a.PROJECT_ID == id);
                }
                else
                {
                    throw new Exception(string.Format("Project with the id - {0} could not be found", id));
                }
            }
        }

        public TASKTABLE GetTaskByID(string id)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                List<TASKTABLE> allTasks = (from task1 in _db.TASKTABLEs select task1).ToList();
                TASKTABLE task = allTasks.Where(a => a.TASK_ID == id).FirstOrDefault();
                return task;
            }
        }

        public bool DeleteProject(string id)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                if (_db.PROJECTs.Any(a => a.PROJECT_ID == id))
                {
                    PROJECT projectToRemove = _db.PROJECTs.FirstOrDefault(a => a.PROJECT_ID == id);
                    _db.PROJECTs.Remove(projectToRemove);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Project with the name - {0} could not be found", id));
                }
            }
        }

        public bool UpdateProject(PROJECT pROJECT)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                if (_db.PROJECTs.Any(a => a.PROJECT_ID == pROJECT.PROJECT_ID))
                {
                    PROJECT projectToRemove = _db.PROJECTs.FirstOrDefault(a => a.PROJECT_ID == pROJECT.PROJECT_ID);
                    _db.PROJECTs.Remove(projectToRemove);
                    _db.PROJECTs.Add(pROJECT);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Project with the name - {0} could not be found", pROJECT.PROJECT1));
                }
            }

           
        }

        public List<ParentTask> GetParentAllTasks()
        {
            using (ProjectManagerEntities pe = new ProjectManagerEntities())
            {
                return pe.ParentTasks.ToList();
            }
        }

        public bool AddTask(TASKTABLE taskToAdd)
        {
            using (ProjectManagerEntities pe = new ProjectManagerEntities())
            {
                pe.TASKTABLEs.Add(new TASKTABLE()
                {
                    TASK = taskToAdd.TASK,
                    TASK_ID = taskToAdd.TASK_ID,
                    START_DATE = taskToAdd.START_DATE,
                    END_DATE = taskToAdd.END_DATE,
                    PRIORITY = taskToAdd.PRIORITY,
                    PROJECT_ID = taskToAdd.PROJECT_ID,
                    STATUS = taskToAdd.STATUS,
                    PARENT_ID = taskToAdd.PARENT_ID

                });
                pe.SaveChanges();

            }

            return true;
        }

        public List<TASKTABLE> GetAllTasks()
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                return _db.TASKTABLEs.ToList();
            }
        }

        public PROJECT GetProjectFromDetails(ProjectDetails projDetails)
        {
            PROJECT retVal = new PROJECT();
            retVal.PROJECT_ID = projDetails.PROJECT_ID;
            retVal.PROJECT1 = projDetails.PROJECT_NAME;
            retVal.START_DATE = projDetails.START_DATE;
            retVal.END_DATE = projDetails.END_DATE;
            retVal.PRIORITY = projDetails.PRIORITY;
            return retVal;
        }

        public USER AddUser(USER userToAdd)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                _db.USERS.Add(new USER()
                {
                    USER_ID = userToAdd.USER_ID,
                    EMPLOYEE_ID = userToAdd.EMPLOYEE_ID,
                    FIRST_NAME = userToAdd.FIRST_NAME,
                    LAST_NAME = userToAdd.LAST_NAME
                });
                _db.SaveChanges();
                return userToAdd;
            }
        }

        public bool DeleteUser(string id)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                if (_db.USERS.Any(a => a.USER_ID == id))
                {
                    USER userToRemove = _db.USERS.FirstOrDefault(a => a.USER_ID == id);
                    _db.USERS.Remove(userToRemove);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("User with the name - {0} could not be found", id));
                }
            }
        }

        public bool DeletUser(string id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(USER updateUser)
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                if (_db.USERS.Any(a => a.USER_ID == updateUser.USER_ID))
                {
                    USER userToRemove = _db.USERS.FirstOrDefault(a => a.USER_ID == updateUser.USER_ID);
                    _db.USERS.Remove(userToRemove);
                    _db.USERS.Add(updateUser);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("User with the name - {0} could not be found", updateUser.USER_ID));
                }
            }
        }

        public List<USER> GetAllUsers()
        {
            using (ProjectManagerEntities _db = new ProjectManagerEntities())
            {
                return _db.USERS.ToList();
            }
        }
    }
}
