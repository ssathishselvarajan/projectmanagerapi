﻿using ProjectManagerEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace ProjectManagerAPIServices.Controllers
{
     public class ValuesController : ApiController
    {
        [Route("api/values/GetAllProjects")]
        [HttpGet]

        public List<PROJECT> GetAllProjects()
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            return pz.GetAllProjects();
        }

        //[Route("api/values/GetTaskDetails")]
        //[HttpGet]
        //public List<GetTasktabledata_Result> GetTaskDetails(Nullable<int> id)

        //{
        //    using (TaskHandlerEntities te = new TaskHandlerEntities())
        //    {
        //        return te.GetTasktabledata(id).ToList();
        //    }
        //}

        //[Route("api/values/UpdateTaskDetails")]
        //[HttpPut]
        //public IHttpActionResult  UpdateTaskDetails(GetTasktabledata_Result taskdata)

        //{
        //    using (TaskHandlerEntities te = new TaskHandlerEntities())
        //    {

        //        var tasktable = te.TaskTables.Where(t => t.Task_ID == taskdata.TaskID).FirstOrDefault<TaskTable>();

        //        if(tasktable!=null)
        //        {
        //            tasktable.Task = taskdata.Task;
        //            tasktable.Priority = taskdata.Priority;
        //            tasktable.TaskStart_Date = taskdata.StartDate;
        //            tasktable.End_Date = taskdata.EndDate;
        //            te.SaveChanges();
        //        }
        //        else
        //        {
        //            return NotFound();
        //        }

        //        var parenttasktable = te.ParentTasks.Where(p => p.Parent_ID == taskdata.ParentTaskID).FirstOrDefault<ParentTask>();

        //        if (parenttasktable != null)
        //        {
        //            parenttasktable.Parent_Task = taskdata.ParentTask;
        //            te.SaveChanges();
        //        }
        //        else
        //        {
        //            return NotFound();
        //        }

        //        return Ok();



        //    }
        //}

        //[Route("api/values/AddTaskDetails")]
        //[HttpPost]
        //public IHttpActionResult AddTaskDetails(GetTasktabledata_Result taskdata)
        //{

        //    if (!ModelState.IsValid)
        //        return BadRequest("Invalid data.");

        //    using (var ctx = new TaskHandlerEntities())
        //    {


        //        ctx.ParentTasks.Add(new ParentTask()
        //        {
        //            Parent_Task = taskdata.ParentTask

        //        }
        //       );

        //        ctx.SaveChanges();

        //        var parenttask = ctx.ParentTasks.Where(t => t.Parent_Task == taskdata.ParentTask).FirstOrDefault<ParentTask>();

        //        ctx.TaskTables.Add(new TaskTable()
        //        {
        //            Task_ID = taskdata.TaskID,
        //            Parent_ID = parenttask.Parent_ID,
        //            Task = taskdata.Task,
        //            TaskStart_Date = taskdata.StartDate,
        //            End_Date = taskdata.EndDate,
        //            Priority = taskdata.Priority
        //        });

        //        ctx.SaveChanges();
        //    }

        //    return Ok();



        //}

        //[Route("api/values/DeleteTask")]
        //[HttpDelete]
        //public IHttpActionResult DeleteTask(int id)
        //{
        //    if (id <= 0)
        //        return BadRequest("Not a valid student id");

        //    using (var ctx = new TaskHandlerEntities())
        //    {

        //        var parenttask = ctx.ParentTasks.Where(p => p.Parent_ID == id).FirstOrDefault<ParentTask>();

        //        ctx.Entry(parenttask).State = System.Data.Entity.EntityState.Deleted;
        //        ctx.SaveChanges();

        //        var tasktable = ctx.TaskTables.Where(t=>t.Parent_ID==parenttask.Parent_ID).FirstOrDefault<TaskTable>();

        //        ctx.Entry(tasktable).State = System.Data.Entity.EntityState.Deleted;
        //        ctx.SaveChanges();
        //    }

        //    return Ok();
        //}


    }
    }

