﻿using ProjectManagerBiz;
using ProjectManagerEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagerAPIServices.Controllers
{
    public class ProjectController : ApiController
    {
      
        [HttpGet]
        [Route("api/project/GetAllProjects")]
        public List<ProjectManagerEntity.PROJECT> GetAllProjects()
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            List<PROJECT> allProjects = pz.GetAllProjects();
            return (allProjects);
        }


        [HttpGet]
        [Route("api/project/GetAllTasksByProjectID")]
        public IHttpActionResult GetAllTasksByProjectID(string id)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            List<TASKTABLE> selectedTask = pz.GetTasksByID(id);
            return Ok(selectedTask);
        }


        [HttpPost]
        [Route("api/project/AddProject")]
        public IHttpActionResult PostProject([FromBody] ProjectDetails projectToAdd)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            projectToAdd.PROJECT_ID = Guid.NewGuid().ToString();
            pz.AddProject(projectToAdd);
            return Ok(projectToAdd);
        }

        [HttpPut]
        [Route("api/project/UpdateProject")]
        public IHttpActionResult UpdateProject([FromBody] ProjectDetails projectToUpdate)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            pz.UpdateProject(pz.GetProjectFromDetails(projectToUpdate));
            return Ok(projectToUpdate);
        }

        [HttpGet]
        [Route("api/project/GetTaskByID")]
        public IHttpActionResult GetTaskByID(string id)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            TASKTABLE selectedTask = pz.GetTaskByID(id);
            return Ok(selectedTask);
        }


        [HttpGet]
        [Route("api/project/GetAllProjectDetails")]
        public IHttpActionResult GetAllProjectDetails()
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            List<ProjectDetails> retVal = new List<ProjectDetails>();
            retVal = pz.GetProjectFromDetails();
            return Ok(retVal);
                                
        }


        [HttpPost]
        [Route("api/project/AddUser")]
        public IHttpActionResult PostUser([FromBody] USER userToAdd)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            userToAdd.USER_ID = Guid.NewGuid().ToString();
            pz.AddUser(userToAdd);
            return Ok(userToAdd);
        }


        [HttpGet]
        [Route("api/project/GetAllUsers")]
        public IHttpActionResult GetAllUsers()
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            List<USER> allUsers = pz.GetAllUsers();
            return Ok(allUsers);
        }


        [HttpPut]
        [Route("api/project/UpdateUser")]
        public IHttpActionResult UpdateUser([FromBody] USER userToUpdate)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            pz.UpdateUser(userToUpdate);
            return Ok(userToUpdate);
        }


        [HttpDelete]
        [Route("api/project/DeleteUser")]
        public IHttpActionResult DeleteUser(string id)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            bool isDeleted =  pz.DeleteUser(id);
            return Ok(isDeleted);
        }

        [HttpGet]
        [Route("api/project/GetAllTasks")]
        public IHttpActionResult GetAllTasks()
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            
            List<TASKTABLE> allTasks = pz.GetAllTasks();
            return Ok(allTasks);
        }

        [HttpGet]
        [Route("api/project/GetParentAllTasks")]
        public IHttpActionResult GetParentAllTasks()
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();

            List<ParentTask> allTasks = pz.GetParentAllTasks();
            return Ok(allTasks);
        }


        [HttpPost]
        [Route("api/project/AddTask")]
        public IHttpActionResult AddTask([FromBody] TASKTABLE taskToAdd)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            taskToAdd.TASK_ID = Guid.NewGuid().ToString();
            pz.AddTask(taskToAdd);
            return Ok(taskToAdd);
        }


        [HttpDelete]
        [Route("api/project/DeleteProject")]
        public IHttpActionResult DeleteProject(string id)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            bool isDeleted = pz.DeleteProject(id);
            return Ok(isDeleted);
        }

        [HttpGet]
        [Route("api/project/GetProjectByID")]
        public IHttpActionResult GetProjectByID(string id)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            PROJECT selectedProject = pz.GetProjectByID(id);
            return Ok(selectedProject);
        }

        [HttpPut]
        [Route("api/project/UpdateTask")]
        public IHttpActionResult UpdateTask([FromBody] TASKTABLE taskToUpdate)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            pz.UpdateTask(taskToUpdate);
            return Ok(taskToUpdate);
        }


        [HttpDelete]
        [Route("api/project/DeleteTask")]
        public IHttpActionResult DeleteTask(string id)
        {
            ProjectManagerBiz.ProjectManagerBiz pz = new ProjectManagerBiz.ProjectManagerBiz();
            bool isDeleted = pz.DeleteTask(id);
            return Ok(isDeleted);
        }
    }
}
