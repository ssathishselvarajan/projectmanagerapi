﻿using System;
using ProjectManagerAPIServices.Controllers;
using System.Web.Http.Results;
using System.Collections.Generic;
using NUnit.Framework;
using ProjectManagerEntity;
using System.Web.Http;

namespace TaskHandlerAPIservice.Tests
{
    [TestFixture]
    public class ProjectControllerTest
    {
        [Test]
        public void Test_GetAllProjects()
        {
            ProjectController tc = new ProjectController();
            var result = tc.GetAllProjects();
            Assert.IsNotNull(result);
           

        }

        [Test]
        public void Test_GetProjectByID()
        {
            ProjectController tc = new ProjectController();
            IHttpActionResult result = tc.GetProjectByID("937b4a3f-1e65-483b-9f93-82b4fe48926d");
            var actual = result as OkNegotiatedContentResult<PROJECT>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }


        //[Test]
        //public void Test_PostProject()
        //{
        //    ProjectController tc = new ProjectController();
        //    ProjectDetails projectToAdd = new ProjectDetails();
        //    projectToAdd.PROJECT_NAME = "Project from Unit Test Project";
        //    projectToAdd.PRIORITY = 50;
        //    projectToAdd.START_DATE = DateTime.Now;
        //    projectToAdd.END_DATE = DateTime.Now.AddDays(2);
        //    projectToAdd.NoOfTasks = 1;
        //    projectToAdd.NoOfTasksCompleted = 0;
        //    projectToAdd.Manager = new USER();
        //    IHttpActionResult result = tc.PostProject(projectToAdd);
        //    var actual = result as OkNegotiatedContentResult<PROJECT>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);

        //}

        //[Test]
        //public void Test_UpdateProject()
        //{
        //    ProjectController tc = new ProjectController();
        //    ProjectDetails projectToAdd = new ProjectDetails();
        //    projectToAdd.PROJECT_ID = "937b4a3f-1e65-483b-9f93-82b4fe48926d";
        //    projectToAdd.PROJECT_NAME = "Project updated from Unit Test Project";
        //    projectToAdd.PRIORITY = 50;
        //    projectToAdd.START_DATE = DateTime.Now;
        //    projectToAdd.END_DATE = DateTime.Now.AddDays(5);
        //    projectToAdd.NoOfTasks = 1;
        //    projectToAdd.NoOfTasksCompleted = 0;
        //    projectToAdd.Manager = new USER();
        //    IHttpActionResult result = tc.UpdateProject(projectToAdd);
        //    var actual = result as OkNegotiatedContentResult<PROJECT>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);

        //}

        //[Test]
        //public void Test_DeleteProject()
        //{
        //    ProjectController tc = new ProjectController();
        //    IHttpActionResult result = tc.DeleteProject("bc36fa90-2180-4ac2-8d68-d5e1d4f5b337");
        //    var actual = result as OkNegotiatedContentResult<bool>;
        //    Assert.AreEqual(actual.Content, true);
        //}

        [Test]
        public void Test_GetAllTasks()
        {
            ProjectController tc = new ProjectController();
            var result = tc.GetAllTasks();
            var actual = result as OkNegotiatedContentResult<List<TASKTABLE>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }

        [Test]
        public void Test_GetTaskByID()
        {
            ProjectController tc = new ProjectController();
            IHttpActionResult result = tc.GetTaskByID("9df2b23d-d8a6-4a9e-a415-a989492a08e2");
            var actual = result as OkNegotiatedContentResult<TASKTABLE>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }


        //[Test]
        //public void Test_PostTask()
        //{
        //    ProjectController tc = new ProjectController();
        //    TASKTABLE taskToAdd = new TASKTABLE();
        //    taskToAdd.TASK_ID = "cv3fa07f-568f-42e6-90db-a3e0bfe3cfc9";
        //    taskToAdd.TASK = "Task from Unit Test Project";
        //    taskToAdd.PRIORITY = 50;
        //    taskToAdd.START_DATE = DateTime.Now;
        //    taskToAdd.END_DATE = DateTime.Now.AddDays(2);
        //    IHttpActionResult result = tc.AddTask(taskToAdd);
        //    var actual = result as OkNegotiatedContentResult<TASKTABLE>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);

        //}

        //[Test]
        //public void Test_UpdateTask()
        //{
        //    ProjectController tc = new ProjectController();
        //    TASKTABLE taskToAdd = new TASKTABLE();
        //    taskToAdd.TASK_ID = "ca3fa07f-568f-42e6-90db-a3e0bfe3cfc9";
        //    taskToAdd.TASK = "Task updated from Unit Test Project";
        //    taskToAdd.PRIORITY = 50;
        //    taskToAdd.START_DATE = DateTime.Now;
        //    taskToAdd.END_DATE = DateTime.Now.AddDays(5);
        //    IHttpActionResult result = tc.UpdateTask(taskToAdd);
        //    var actual = result as OkNegotiatedContentResult<TASKTABLE>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);

        //}

        //[Test]
        //public void Test_DeleteTask()
        //{
        //    ProjectController tc = new ProjectController();
        //    IHttpActionResult result = tc.DeleteTask("ca3fa07f-568f-42e6-90db-a3e0bfe3cfc9");
        //    var actual = result as OkNegotiatedContentResult<bool>;
        //    Assert.AreEqual(actual.Content, true);
        //}

        [Test]
        public void Test_GetAllUsers()
        {
            ProjectController tc = new ProjectController();
            var result = tc.GetAllUsers();
            var actual = result as OkNegotiatedContentResult<List<USER>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }

        //[Test]
        //public void Test_GetUserByID()
        //{
        //    ProjectController tc = new ProjectController();
        //    IHttpActionResult result = tc.get("00f4f0a2-8c15-4ded-9577-a09baa935863");
        //    var actual = result as OkNegotiatedContentResult<USER>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);

        //}


        [Test]
        public void Test_PostUser()
        {
            ProjectController tc = new ProjectController();
            USER userToAdd = new USER();
            userToAdd.FIRST_NAME = "User FN from Unit Test Project";
            userToAdd.EMPLOYEE_ID = 50;
            userToAdd.LAST_NAME = "User LN from Unit Test Project";
            IHttpActionResult result = tc.PostUser(userToAdd);
            var actual = result as OkNegotiatedContentResult<USER>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }

        //[Test]
        //public void Test_UpdateUser()
        //{
        //    ProjectController tc = new ProjectController();
        //    USER userToAdd = new USER();
        //    userToAdd.USER_ID = "a0d098af-00fe-490e-8e57-92dcfb1e5eed";
        //    userToAdd.FIRST_NAME = "User FN updated from Unit Test Project";
        //    userToAdd.EMPLOYEE_ID = 60;
        //    userToAdd.LAST_NAME = "User LN updated from Unit Test Project";
        //    IHttpActionResult result = tc.UpdateUser(userToAdd);
        //    var actual = result as OkNegotiatedContentResult<TASKTABLE>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);

        //}

       
    }
}
