﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagerEntity;

namespace ProjectManagerBiz
{
    public class ProjectManagerBiz
    {
        public List<PROJECT> GetAllProjects()
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetAllProjects();
        }

        public List<TASKTABLE> GetTasksByID(string id)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetTasksByID(id);
        }

        public bool AddProject(ProjectDetails procdetails)
        {

            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.AddProject(procdetails);
        }


        public PROJECT GetProjectFromDetails(ProjectDetails projDetails)
        {
            PROJECT retVal = new PROJECT();
            retVal.PROJECT_ID = projDetails.PROJECT_ID;
            retVal.PROJECT1 = projDetails.PROJECT_NAME;
            retVal.START_DATE = projDetails.START_DATE;
            retVal.END_DATE = projDetails.END_DATE;
            retVal.PRIORITY = projDetails.PRIORITY;
            return retVal;
        }

        public USER AddUser(USER userToAdd)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.AddUser(userToAdd);
        }

        public List<USER> GetAllUsers()
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return  pd.GetAllUsers();
        }

        public bool UpdateProject(PROJECT pROJECT)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.UpdateProject(pROJECT);
        }

        public List<ProjectDetails> GetProjectFromDetails()
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetProjectFromDetails();
        }

        public TASKTABLE GetTaskByID(string id)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetTaskByID(id);
        }

        public bool UpdateUser(USER userToUpdate)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.UpdateUser(userToUpdate);
        }

        public bool DeleteUser(string id)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.DeleteUser(id);
        }

        public List<TASKTABLE> GetAllTasks()
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetAllTasks();
           
        }

        public bool AddTask(TASKTABLE taskToAdd)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.AddTask(taskToAdd);
        }

        public List<ParentTask> GetParentAllTasks()
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetParentAllTasks();
        }

        public bool DeleteProject(string id)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.DeleteProject(id);
        }

        public PROJECT GetProjectByID(string id)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.GetProjectByID(id);
        }

        public bool UpdateTask(TASKTABLE taskToUpdate)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.UpdateTask(taskToUpdate);
        }

        public bool DeleteTask(string id)
        {
            ProjectManagerData.ProjectManagerData pd = new ProjectManagerData.ProjectManagerData();
            return pd.DeleteTask(id);
        }
    }


    
}
