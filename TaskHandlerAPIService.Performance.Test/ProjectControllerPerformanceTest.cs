﻿using NBench;
using System;
using ProjectManagerAPIServices.Controllers;

namespace TaskHandlerAPIService.Performance.Test
{
    
    public class ProjectControllerPerformanceTest
    {
        [PerfBenchmark(Description = "Performace Test for GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestProjectCounter")]
        public void GetAllProjects_PassingTest()
        {
            ProjectController controller = new ProjectController();
            System.Collections.Generic.List<ProjectManagerEntity.PROJECT> result = controller.GetAllProjects();
        }

        [PerfBenchmark(Description = "Performace Test for GET (By ID)", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestProjectCounter")]
        public void GetProjectByID_PassingTest()
        {
            ProjectController tc = new ProjectController();
            tc.GetProjectByID("8e769139-a5c6-4330-a9da-ca769bbe64fc");
        }

        [PerfBenchmark(Description = "Performace Test for POST", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestProjectCounter")]
        public void PostProject_PassingTest()
        {
            ProjectController tc = new ProjectController();
            ProjectManagerEntity.ProjectDetails projectToAdd = new ProjectManagerEntity.ProjectDetails()
            {
                PROJECT_NAME = "Project from Performance Unit Test Project",
                PRIORITY = 50,
                START_DATE = DateTime.Now,
                END_DATE = DateTime.Now.AddDays(2),
                NoOfTasks = 1,
                NoOfTasksCompleted = 0,
                Manager = new ProjectManagerEntity.USER()
            };
            tc.PostProject(projectToAdd);

        }

        [PerfBenchmark(Description = "Performace Test for UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestProjectCounter")]
        public void UpdateProject_PassingTest()
        {
            ProjectController tc = new ProjectController();
            ProjectManagerEntity.ProjectDetails projectToAdd = new ProjectManagerEntity.ProjectDetails();
            projectToAdd.PROJECT_ID = "c4373cc2-386a-4551-a084-e55bd10864d0";
            projectToAdd.PROJECT_NAME = "Project updated from Performance Unit Test Project";
            projectToAdd.PRIORITY = 50;
            projectToAdd.START_DATE = DateTime.Now;
            projectToAdd.END_DATE = DateTime.Now.AddDays(5);
            projectToAdd.NoOfTasks = 1;
            projectToAdd.NoOfTasksCompleted = 0;
            projectToAdd.Manager = new ProjectManagerEntity.USER();
            tc.UpdateProject(projectToAdd);

        }



    }
}
