﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManagerEntity;

namespace ProjectManagerData
{
    public class ProjectManagerData
    {
        public List<PROJECT> GetAllProjects()
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetAllProjects();
        }

        public List<TASKTABLE> GetTasksByID(string id)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetTasksByID(id);
        }

        public bool AddProject(ProjectDetails procdetails)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.AddProject(procdetails);
        }

        public USER AddUser(USER userToAdd)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.AddUser(userToAdd);
        }

        public List<USER> GetAllUsers()
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetAllUsers();
        }

        public bool UpdateUser(USER userToUpdate)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.UpdateUser(userToUpdate);
        }

        public bool DeleteUser(string id)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.DeleteUser(id);
        }

        public List<ProjectDetails> GetProjectFromDetails()
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetProjectFromDetails();
        }

        public bool UpdateProject(PROJECT pROJECT)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.UpdateProject(pROJECT);
        }

        public List<TASKTABLE> GetAllTasks()
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetAllTasks();
        }

        public TASKTABLE GetTaskByID(string id)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetTaskByID(id);
        }

        public bool AddTask(TASKTABLE taskToAdd)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.AddTask(taskToAdd);
        }

        public List<ParentTask> GetParentAllTasks()
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetParentAllTasks();
        }

        public bool DeleteProject(string id)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.DeleteProject(id);
        }

        public PROJECT GetProjectByID(string id)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.GetProjectByID(id);
        }

        public bool UpdateTask(TASKTABLE taskToUpdate)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.UpdateTask(taskToUpdate);
        }

        public bool DeleteTask(string id)
        {
            ProjectManagerEntity.ProjectManagerEntity pe = new ProjectManagerEntity.ProjectManagerEntity();
            return pe.DeleteTask(id);
        }
    }
}
